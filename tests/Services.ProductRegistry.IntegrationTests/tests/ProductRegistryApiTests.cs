﻿using Common.DTOs.ApiRequests.Products;
using Common.DTOs.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ProductRegistry.IntegrationTests.tests
{
    public class ProductRegistryApiTests : BaseTest
    {
        public ProductRegistryApiTests(ProductRegistryIntegrationTestApiFactory factory)
            : base(factory)
        { }

        [Fact]
        public async Task CreateSeller_WhenInputIsInvalid_ReturnsFailBadRequest()
        {
            // arrange
            ProductSellerApiPost model1 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = null,
                Email = null,
                Site = null,
                Title = null,
            };

            ProductSellerApiPost model2 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = null,
                Email = null,
                Site = null,
                Title = "Simple good title",
            };

            ProductSellerApiPost model3 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = string.Empty,
                Email = null,
                Site = null,
                Title = string.Empty,
            };

            ProductSellerApiPost model4 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = "91238010939139010good-special-code",
                Email = null,
                Site = null,
                Title = string.Empty,
            };

            ProductSellerApiPost model5 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = new string('a', 400),
                Email = null,
                Site = null,
                Title = new string('a', 400)
            };

            // act
            var resp1 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model1);

            var resp2 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model2);

            var resp3 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model3);

            var resp4 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model4);

            var resp5 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model5);

            // assert
            Assert.False(resp1.IsSuccessed);
            Assert.Null(resp1.Value);
            Assert.NotEmpty(resp1.Errors);

            Assert.False(resp2.IsSuccessed);
            Assert.Null(resp2.Value);
            Assert.NotEmpty(resp2.Errors);

            Assert.False(resp3.IsSuccessed);
            Assert.Null(resp3.Value);
            Assert.NotEmpty(resp3.Errors);

            Assert.False(resp4.IsSuccessed);
            Assert.Null(resp4.Value);
            Assert.NotEmpty(resp4.Errors);

            Assert.False(resp5.IsSuccessed);
            Assert.Null(resp5.Value);
            Assert.NotEmpty(resp5.Errors);
        }

        [Fact]
        public async Task CreateSeller_WhenInputIsValid_ReturnsOkResult()
        {
            // arrange
            ProductSellerApiPost model1 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = "Good come sample",
                Email = null,
                Site = null,
                Title = "Good title sample",
            };

            ProductSellerApiPost model2 = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = new string('a', 120),
                Email = null,
                Site = null,
                Title = new string('b', 50),
            };

            // act
            var resp1 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model1);

            var resp2 = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: model2);

            // assert
            Assert.True(resp1.IsSuccessed);
            Assert.NotNull(resp1.Value);
            Assert.Empty(resp1.Errors);
            Assert.True(DbContext.ProductSellers.Any(x => x.Id == resp1.Value.Id));

            Assert.True(resp2.IsSuccessed);
            Assert.NotNull(resp2.Value);
            Assert.Empty(resp2.Errors);
            Assert.True(DbContext.ProductSellers.Any(x => x.Id == resp2.Value.Id));
        }

        [Fact]
        public async Task CreateProduct_WhenInputIsInvalid_ReturnsFailBadRequest()
        {
            // arrange
            ProductApiPost model1 = new ProductApiPost
            {
                DefaultPrice = -1, // bad
                Description = null,
                SellerId = null, // bad
                Title = "Good title"
            };

            ProductApiPost model2 = new ProductApiPost
            {
                DefaultPrice = 200,
                Description = null,
                SellerId = null, // bad
                Title = "Good title"
            };

            ProductApiPost model3 = new ProductApiPost
            {
                DefaultPrice = 200, // bad
                Description = null,
                SellerId = null, // bad
                Title = null // bad
            };

            ProductApiPost model4 = new ProductApiPost
            {
                DefaultPrice = 200,
                Description = null,
                SellerId = "1d09ud", // bad
                Title = null // bad
            };

            ProductApiPost model5 = new ProductApiPost
            {
                DefaultPrice = 200,
                Description = null,
                SellerId = Guid.NewGuid().ToString(),
                Title = new string('a', 300) // bad
            };

            // act
            var resp1 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: model1);

            var resp2 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: model2);

            var resp3 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: model3);

            var resp4 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: model4);

            var resp5 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: model5);

            // assert
            Assert.False(resp1.IsSuccessed);
            Assert.Null(resp1.Value);
            Assert.NotEmpty(resp1.Errors);

            Assert.False(resp2.IsSuccessed);
            Assert.Null(resp2.Value);
            Assert.NotEmpty(resp2.Errors);

            Assert.False(resp3.IsSuccessed);
            Assert.Null(resp3.Value);
            Assert.NotEmpty(resp3.Errors);

            Assert.False(resp4.IsSuccessed);
            Assert.Null(resp4.Value);
            Assert.NotEmpty(resp4.Errors);

            Assert.False(resp5.IsSuccessed);
            Assert.Null(resp5.Value);
            Assert.NotEmpty(resp5.Errors);
        }

        [Fact]
        public async Task CreateProduct_WhenInputIsValid_ReturnsOkResult()
        {
            // arrange
            ProductSellerApiPost seller = new ProductSellerApiPost
            {
                Bank = null,
                Description = null,
                SpecialCode = "Good come sample",
                Email = null,
                Site = null,
                Title = "Good title sample",
            };

            var sellerResponse = await ApiPostAsync<ProductSellerRead, ProductSellerApiPost>(
                uri: "/sellers",
                body: seller);

            ProductApiPost product = new ProductApiPost
            {
                DefaultPrice = 150,
                Description = null,
                SellerId = sellerResponse.Value.Id,
                Title = "Good title"
            };

            // act
            var resp1 = await ApiPostAsync<ProductShortRead, ProductApiPost>(
                uri: "/",
                body: product);

            // assert
            Assert.True(resp1.IsSuccessed);
            Assert.NotNull(resp1.Value);
            Assert.Empty(resp1.Errors);
            Assert.True(DbContext.Products.Any(x => x.Id == resp1.Value.Id));
        }
    }
}
