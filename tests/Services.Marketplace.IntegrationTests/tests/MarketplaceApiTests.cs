﻿using Common.DataQueries;
using Common.DTOs.ApiRequests;
using Common.DTOs.Catalog.Catalogs;
using Common.DTOs.Catalog.Properties;
using Common.DTOs.Catalog.Units;
using Common.DTOs.Marketplace;
using Common.DTOs.Marketplace.Api;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.PropertyEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Marketplace.IntegrationTests.tests
{
    public class MarketplaceApiTests : BaseTest
    {
        public MarketplaceApiTests(MarketplaceServiceIntergrationTestApiFactory factory) 
            : base(factory)
        {}

        #region Upd-1

        [Fact]
        public async Task CreateCatalog_InputIsInvalid_ReturnsFailOkResult()
        {
            // arrange
            CatalogApiCreate model1 = new CatalogApiCreate
            {
                Name = null
            };

            CatalogApiCreate model2 = new CatalogApiCreate
            {
                Name = string.Empty
            };

            CatalogApiCreate model3 = new CatalogApiCreate
            {
                Name = new string('a', 200)
            };

            // act
            ApiResponseRead<Catalog> resp1 = await ApiPostAsync<Catalog, CatalogApiCreate>(
                uri: "/",
                body: model1);

            ApiResponseRead<Catalog> resp2 = await ApiPostAsync<Catalog, CatalogApiCreate>(
               uri: "/",
               body: model2);

            ApiResponseRead<Catalog> resp3 = await ApiPostAsync<Catalog, CatalogApiCreate>(
               uri: "/",
               body: model3);

            // asset
            Assert.False(resp1.IsSuccessed);
            Assert.NotEmpty(resp1.Errors);
            Assert.Null(resp1.Value);

            Assert.False(resp2.IsSuccessed);
            Assert.NotEmpty(resp2.Errors);
            Assert.Null(resp2.Value);

            Assert.False(resp3.IsSuccessed);
            Assert.NotEmpty(resp3.Errors);
            Assert.Null(resp3.Value);
        }

        [Fact]
        public async Task CreateCatalog_InputIsValid_ReturnsOkResult()
        {
            // arrange
            CatalogApiCreate model1 = new CatalogApiCreate
            {
                Name = "test-catalog"
            };

            // act
            ApiResponseRead<Catalog> resp1 = await ApiPostAsync<Catalog, CatalogApiCreate>(
                uri: "/",
                body: model1);

            // asset
            Assert.True(resp1.IsSuccessed);
            Assert.Empty(resp1.Errors);
            Assert.NotNull(resp1.Value);
            Assert.True(DbContext.Catalogs.Any(x => x.Name == resp1.Value.Name));
        }

        [Fact]
        public async Task CreateProduct_InputIsValid_ReturnsOkResult()
        {
            // arrange

            CatalogPropertyUnitApiCreate unit1 = new CatalogPropertyUnitApiCreate
            {
                Name = "kg"
            };
            var unitResp = await ApiPostAsync<CatalogPropertyUnit, CatalogPropertyUnitApiCreate>(
                uri: "/properties/units",
                body: unit1);

            CatalogPropertyApiCreate prop1 = new CatalogPropertyApiCreate
            {
                UnitId = unitResp.Value.Id,
                Desc = "just weight",
                Name = "Weight"
            };
            var propResp = await ApiPostAsync<CatalogProperty, CatalogPropertyApiCreate>(
                uri: "/properties",
                body: prop1);

            CatalogApiCreate catalog = new CatalogApiCreate
            {
                Name = "test-catalog"
            };
            var catalogResp = await ApiPostAsync<Catalog, CatalogApiCreate>(
                uri: "/",
                body: catalog);

            CategoryApiPost category = new CategoryApiPost
            {
                CatalogId = catalogResp.Value.Id,
                Name = "test-category"
            };
            var categoryResp = await ApiPostAsync<CategoryReadDto, CategoryApiPost>(
                uri: "/categories",
                body: category);

            CategorySectionApiPost section = new CategorySectionApiPost
            {
                CategoryId = categoryResp.Value.Id,
                Name = "test-section"
            };
            var sectionResp = await ApiPostAsync<CategorySectionRead, CategorySectionApiPost>(
                uri: "/sections",
                body: section);

            CatalogProductApiPost model1 = new CatalogProductApiPost
            {
                Title = "test-product-title",
                ExternalProductId = Guid.NewGuid().ToString(),
                Properties = new ProductPropertyApiPost[] { 
                    new ProductPropertyApiPost
                    { 
                        PropertyId = propResp.Value.Id,
                        Value = "24"
                    }
                },
                SectionId = sectionResp.Value.Id
            };

            // act
            ApiResponseRead<string> resp1 = await ApiPostAsync<string, CatalogProductApiPost>(
                uri: "/products",
                body: model1);

            // asset
            Assert.True(resp1.IsSuccessed);
            Assert.Empty(resp1.Errors);
            Assert.NotNull(resp1.Value);
            Assert.True(DbContext.CatalogProducts.Any(x => x.Id == resp1.Value));
        }

        #endregion
    }
}
