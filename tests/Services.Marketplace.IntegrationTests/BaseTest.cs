﻿using Common.DataQueries;
using Marketplace.Data.Context;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Services.Marketplace.IntegrationTests
{
    public abstract class BaseTest : IClassFixture<MarketplaceServiceIntergrationTestApiFactory>
    {
        protected readonly ApplicationContext DbContext;

        protected readonly HttpClient ApiClient;

        protected async Task<ApiResponseRead<TResponse>> ApiPostAsync<TResponse, TMessage>(
            string uri,
            TMessage body)
        {
            var response = await ApiClient.PostAsJsonAsync(uri, body);

            try
            {
                var parse = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponseRead<TResponse>>(
                response.Content.ReadAsStringAsync().Result);

                return parse;
            }
            catch (Exception exp)
            {
                return default(ApiResponseRead<TResponse>);
            }
        }


        protected BaseTest(MarketplaceServiceIntergrationTestApiFactory factory)
        {
            var scope = factory.Services.CreateScope();

            DbContext = scope.ServiceProvider.GetRequiredService<ApplicationContext>();

            ApiClient = factory.CreateClient();
        }
    }
}
