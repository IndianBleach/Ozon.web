﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Marketplace.Api
{
    public class CategoryPropertyApiPost
    {
        public string? CategoryId { get; set; }

        public string? PropertyId { get; set; }

        public bool Required { get; set; }
    }
}
