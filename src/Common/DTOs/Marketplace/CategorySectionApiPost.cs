﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Marketplace
{
    public class CategorySectionApiPost
    {
        public string? CategoryId { get; set; }

        public string? Name { get; set; }
    }
}
