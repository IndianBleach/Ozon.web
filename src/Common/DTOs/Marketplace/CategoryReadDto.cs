﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Marketplace
{
    public class CategoryReadDto
    {
        public string? Id { get; set; }

        public string? CatalogId { get; set; }

        public string? Name { get; set; }
    }
}
