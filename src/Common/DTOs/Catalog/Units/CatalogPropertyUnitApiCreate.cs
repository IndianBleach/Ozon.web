﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Catalog.Units
{
    public class CatalogPropertyUnitApiCreate
    {
        public string? Name { get; set; }
    }
}
