﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Catalog.Catalogs
{
    public class CatalogApiCreate
    {
        public string? Name { get; set; }
    }
}
