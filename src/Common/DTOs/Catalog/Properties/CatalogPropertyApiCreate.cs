﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs.Catalog.Properties
{
    public class CatalogPropertyApiCreate
    {
        public string? Name { get; set; }
            
        public string? UnitId { get; set; }

        public string? Desc { get; set; }
    }
}
