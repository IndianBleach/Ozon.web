using AutoMapper;
using Common.DataQueries;
using Common.DTOs.ApiRequests.Products;
using Common.DTOs.Products;
using Common.Repositories;
using Confluent.Kafka;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Ozon.Bus;
using Ozon.Bus.DTOs.ProductsRegistry;
using Products.Api.Validations;
using Products.Data.Entities;
using Products.Infrastructure.Mappers;
using Products.Infrastructure.Services;

namespace Products.Api.Controllers
{
    //[ApiController]
    [Route("[controller]")]
    //[Produces("application/json")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;

        private readonly IProductService _productService;

        private readonly IProducerFactory _producerFactory;

        public ProductsController(
            IProducerFactory producerFactory,
            IProductService productService,
            ILogger<ProductsController> logger)
        {
            _producerFactory = producerFactory;
            _productService = productService;
            _logger = logger;
        }

        #region Upd-1 (Services)

        [HttpPost("/")]
        public async Task<IActionResult> RegisterProduct(
            [FromBody] ProductApiPost model)
        {
            ValidationResult validationResult = ValidationRegistry.CreateProductValidator
                .Validate(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<ProductShortRead>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            QueryResult<ProductShortRead> prodResult = await _productService.CreateProductAsync(model);

            //counter

            return Ok(prodResult.ToApiResponse());
        }

        [HttpPost("/sellers")]
        public async Task<IActionResult> RegisterSeller(
            [FromBody] ProductSellerApiPost seller)
        {
            ValidationResult validationResult = ValidationRegistry.CreateSellerValidator.Validate(seller);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<ProductSellerRead>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            QueryResult<ProductSellerRead> result = await _productService.CreateSellerAsync(seller);

            if (result.IsSuccessed && result.Value != null)
            {
                ProducerWrapper<string, ProductRegistryMarketplaceSeller>? producer = _producerFactory.Get<string, ProductRegistryMarketplaceSeller>();

                if (producer != null)
                {
                    producer.PublishMessage(
                        toTopicAddr: "products-marketplace.addMarketplaceSeller",
                        message: new Message<string, ProductRegistryMarketplaceSeller>
                        {
                            Key = Guid.NewGuid().ToString(),
                            Value = new ProductRegistryMarketplaceSeller
                            {
                                Description = seller.Description,
                                Email = seller.Email,
                                ExternalSellerId = result.Value.Id,
                                Name = seller.Title,
                                Site = seller.Site
                            }
                        },
                        handler: (report) =>
                        {
                            _logger.LogInformation($"msg[products-marketplace.addMarketplaceSeller] report: {report.Error.Reason} {report.Status.ToString()}");
                        });
                }
                else _logger.LogCritical("producer not found [ProductRegistryMarketplaceSeller]");
            }

            return Ok(result.ToApiResponse());
        }

        [HttpGet("/sellers")]
        public async Task<IActionResult> GetAllSellers()
        {
            return Ok(_productService.GetAllProductSellers());
        }

        [HttpGet("/")]
        public async Task<IActionResult> GetAllProducts()
        {
            return Ok(_productService.GetAllProducts());
        }

        #endregion
    }
}