﻿namespace Products.Api.Validations
{
    public static class ValidationRegistry
    {
        public static CreateSellerValdiator CreateSellerValidator = new CreateSellerValdiator();

        public static CreateProductValidator CreateProductValidator = new CreateProductValidator();
    }
}
