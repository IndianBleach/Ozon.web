﻿using Common.DTOs.ApiRequests.Products;
using FluentValidation;

namespace Products.Api.Validations
{
    public class CreateSellerValdiator : AbstractValidator<ProductSellerApiPost>
    {
        public CreateSellerValdiator()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(120)
                .WithMessage("title is not correct");

            RuleFor(x => x.SpecialCode)
                .Must(d => !string.IsNullOrEmpty(d) && d.Length <= 220)
                .WithMessage("special code can contains 220 symbols");
        }
    }
}
