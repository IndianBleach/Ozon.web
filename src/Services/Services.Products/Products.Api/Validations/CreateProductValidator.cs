﻿using Common.DTOs.ApiRequests.Products;
using FluentValidation;

namespace Products.Api.Validations
{
    public class CreateProductValidator : AbstractValidator<ProductApiPost>
    {
        public CreateProductValidator()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(120)
                .WithMessage("title is not correct");

            RuleFor(x => x.DefaultPrice)
                .NotNull()
                .LessThan(999_999)
                .GreaterThan(9)
                .WithMessage("price is not correct");

            RuleFor(x => x.SellerId)
                .NotNull()
                .Must(id => Guid.TryParse(id, out _))
                .WithMessage("seller is not correct");
        }
    }
}
