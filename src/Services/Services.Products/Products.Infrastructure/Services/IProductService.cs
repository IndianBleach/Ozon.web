﻿using Common.DataQueries;
using Common.DTOs.ApiRequests.Products;
using Common.DTOs.Products;
using Products.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Products.Infrastructure.Services
{
    public interface IProductService
    {

        IEnumerable<ProductRead> GetAllProducts();

        IEnumerable<ProductSeller> GetAllProductSellers();

        Task<QueryResult<ProductShortRead>> CreateProductAsync(ProductApiPost model);

        Task<QueryResult<ProductSellerRead>> CreateSellerAsync(ProductSellerApiPost model);
    }
}
