﻿using AutoMapper;
using Common.DataQueries;
using Common.DTOs.ApiRequests.Products;
using Common.DTOs.Products;
using Common.Repositories;
using Products.Data.Entities;
using Products.Infrastructure.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Products.Infrastructure.Services
{
    public class ProductService : IProductService
    {
        private readonly IServiceAsyncRepository<Product> _productRepository;

        private readonly IServiceAsyncRepository<ProductSeller> _sellerRepository;

        private readonly IMapper _mapper;

        public ProductService(
            IServiceAsyncRepository<ProductSeller> sellerRepository,
            IServiceAsyncRepository<Product> productRepository)
        {
            _sellerRepository = sellerRepository;
            _productRepository = productRepository;

            var config = new MapperConfiguration(cfg => cfg.AddProfiles(new List<Profile> {
                new ProductMapperProfile()
            }));

            _mapper = new Mapper(config);
        }

        public async Task<QueryResult<ProductShortRead>> CreateProductAsync(ProductApiPost model)
        {
            Product product = new Product(
                title: model.Title,
                description: model.Description,
                defaultPrice: model.DefaultPrice,
                dateCreated: DateTime.Now,
                dateUpdated: DateTime.Now,
                sellerId: model.SellerId);

            var query = await _productRepository.CreateAsync(product);

            if (!query.IsSuccessed)
                return QueryResult<ProductShortRead>.Failure(query.StatusMessage);

            return QueryResult<ProductShortRead>.Successed(new ProductShortRead
            {
                Id = query.Value.Id,
                Title = query.Value.Title,
                Description = query.Value.Description
            });
        }

        public async Task<QueryResult<ProductSellerRead>> CreateSellerAsync(ProductSellerApiPost model)
        {
            var query = await _sellerRepository.CreateAsync(new ProductSeller(
                name: model.Title,
                bankAccountNumber: model.Bank,
                site: model.Site,
                contactEmail: model.Email,
                specialNumber: model.SpecialCode,
                description: model.Description,
                dateCreated: DateTime.Now));

            if (!query.IsSuccessed)
                return QueryResult<ProductSellerRead>.Failure(query.StatusMessage);

            return QueryResult<ProductSellerRead>.Successed(new ProductSellerRead
            {
                Id = query.Value.Id,
                Name = query.Value.Name
            });
        }

        public IEnumerable<ProductRead> GetAllProducts()
        {
            var all = _productRepository.GetAll();

            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductRead>>(all);
        }

        public IEnumerable<ProductSeller> GetAllProductSellers()
        {
            return _sellerRepository.GetAll();
        }
    }
}
