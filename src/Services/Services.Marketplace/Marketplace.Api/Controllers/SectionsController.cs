﻿using AutoMapper;
using Common.DataQueries;
using Common.DTOs.Catalog;
using Common.DTOs.Marketplace;
using Common.DTOs.Marketplace.Api;
using Common.Repositories;
using Marketplace.Api.Validations;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.PropertyEntities;
using Marketplace.Infrastructure.Mappers;
using Marketplace.Infrastructure.Services;
using Marketplace.Infrastructure.Specifications.Properties;
using Marketplace.Infrastructure.Specifications.Sections;
using Microsoft.AspNetCore.Mvc;

namespace Marketplace.Api.Controllers
{
    //[ApiController]
    [Route("/[controller]")]
    [Produces("application/json")]
    public class SectionsController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ILogger<SectionsController> _logger;

        private readonly ICatalogService _catalogService;

        public SectionsController(
            ICatalogService catalogService,
            ILogger<SectionsController> logger)
        {
            _catalogService = catalogService;
            _logger = logger;

            var config = new MapperConfiguration(cfg => cfg.AddProfiles(new List<Profile> {
                new CatalogMapProfile(),
                new PropertyMapProfile()
            }));

            _mapper = new Mapper(config);
        }

        #region Upd-1 (Services)

        [HttpGet("/[controller]")]
        public async Task<IActionResult> GetSections()
        {
            _logger.LogInformation(nameof(GetSections));

            return Ok(_catalogService.GetAllSections());
        }

        [HttpPost("/[controller]/properties")]
        public async Task<IActionResult> CreateSectionProperty(
            [FromBody] SectionPropertyApiPost model)
        {
            _logger.LogInformation(nameof(CreateSectionProperty));

            var validationResult = await ValidationRegistry.SectionPropertyCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<SectionProperty>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            var queryResult = await _catalogService.CreateSectionPropertyAsync(model);

            return Ok(queryResult.ToApiResponse());
        }

        [HttpPost("/[controller]")]
        public async Task<IActionResult> CreateSection(
            [FromBody] CategorySectionApiPost model)
        {
            _logger.LogInformation(nameof(CreateSection));

            var validationResult = await ValidationRegistry.CategorySectionCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<CategorySectionRead>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            var queryResult = await _catalogService.CreateCategorySectionAsync(model);

            return Ok(queryResult.ToApiResponse());
        }

        #endregion
    }
}
