﻿using AutoMapper;
using Common.DataQueries;
using Common.DTOs.ApiRequests;
using Common.DTOs.Catalog;
using Common.Repositories;
using Confluent.Kafka;
using Marketplace.Api.Validations;
using Marketplace.Data.Entities.ProductsEntities;
using Marketplace.Data.Entities.PropertyEntities;
using Marketplace.Infrastructure.Mappers;
using Marketplace.Infrastructure.Repositories.Products;
using Marketplace.Infrastructure.Services;
using Marketplace.Infrastructure.Specifications.Properties;
using Microsoft.AspNetCore.Mvc;
using Ozon.Bus;
using Ozon.Bus.DTOs.StorageService;

namespace Marketplace.Api.Controllers
{
    //[ApiController]
    [Route("/[controller]")]
    [Produces("application/json")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;

        private IProducerFactory _producerFactory;

        private IProductService _productService;

        public ProductsController(
            IProductService productService,
            IProducerFactory producerFactory,
            ILogger<ProductsController> logger)
        {
            _producerFactory = producerFactory;
            _productService = productService;
            _logger = logger;
        }

        // validation
        // elastic
        // cache

        #region Upd-1 (Services)

        [HttpGet("/[controller]/")]
        public async Task<IActionResult> GetAllProducts()
        {
            _logger.LogInformation(nameof(GetAllProducts));

            var query = _productService.GetAllProducts();

            return Ok(query);
        }

        [HttpGet("/[controller]/{product_id}")]
        public async Task<IActionResult> GetProductDetail(
            string product_id)
        {
            _logger.LogInformation(nameof(GetProductDetail), product_id);

            var query = await _productService.GetProductDetailByIdAsync(product_id);

            return Ok(query.ToApiResponse());
        }

        [HttpPost("/[controller]/")]
        public async Task<IActionResult> CreateProduct(
            [FromBody] CatalogProductApiPost model)
        {
            var validationResult = await ValidationRegistry.ProductCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<SectionProperty>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            var result = await _productService.CreateProductAsync(model);

            if (result.IsSuccessed && !string.IsNullOrEmpty(result.Value))
            {
                Console.WriteLine("send productRegistry sync");

                ProducerWrapper<string, SyncProductRegistryInfoRequest>? producer = _producerFactory.Get<string, SyncProductRegistryInfoRequest>();
                if (producer != null)
                {
                    _logger.LogInformation($"[syncProductRegistryInfo-req] +msg " + result.Value + " " + model.ExternalProductId);

                    producer.PublishMessage(
                        toTopicAddr: "marketplace-products.syncProductRegistryInfo-req",
                        message: new Message<string, SyncProductRegistryInfoRequest>
                        {
                            Key = Guid.NewGuid().ToString(),
                            Value = new SyncProductRegistryInfoRequest
                            {
                                ExternalProductId = model.ExternalProductId,
                                MarketplaceProductId = result.Value,
                                BusAsnwerChannel = "marketplace-products.syncProductRegistryInfo-answer"
                            }
                        }, (report) => {
                            _logger.LogInformation($"[syncProductRegistryInfo-req] delivery {report.Status}");
                        });
                }
            }

            return Ok(result.ToApiResponse());
        }

        #endregion
    }
}
