﻿using AutoMapper;
using Common.DataQueries;
using Common.DTOs.Catalog;
using Common.DTOs.Marketplace;
using Common.DTOs.Marketplace.Api;
using Common.Repositories;
using Marketplace.Api.Validations;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.PropertyEntities;
using Marketplace.Infrastructure.Mappers;
using Marketplace.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace Marketplace.Api.Controllers
{
    //[ApiController]
    [Route("/[controller]")]
    [Produces("application/json")]
    public class CategoriesController : ControllerBase
    {
        private readonly ILogger<CategoriesController> _logger;

        private readonly ICatalogService _catalogService;

        public CategoriesController(
            ICatalogService catalogService,
            ILogger<CategoriesController> logger)
        {
            _catalogService = catalogService;
            _logger = logger;
        }

        #region Upd-1 (Services)

        [HttpGet("/[controller]")]
        public async Task<IActionResult> AllCategories()
        {
            _logger.LogInformation(nameof(CreateCategory));

            return Ok(_catalogService.GetAllCategories());
        }

        [HttpPost("/[controller]/properties")]
        public async Task<IActionResult> CreateCategoryProperty(
            [FromBody] CategoryPropertyApiPost model)
        {
            _logger.LogInformation(nameof(CreateCategoryProperty));

            var validationResult = await ValidationRegistry.CategoryPropertyCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<CategoryProperty>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            var res = await _catalogService.CreateCategoryPropertyAsync(model);

            return Ok(res.ToApiResponse());
        }

        [HttpPost("/[controller]")]
        public async Task<IActionResult> CreateCategory(
            [FromBody]CategoryApiPost model)
        {
            _logger.LogInformation(nameof(CreateCategory));

            var validationResult = await ValidationRegistry.CategoryCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<CategoryReadDto>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            var query = await _catalogService.CreateCategoryAsync(model);

            return Ok(query.ToApiResponse());
        }

        #endregion
    }
}
