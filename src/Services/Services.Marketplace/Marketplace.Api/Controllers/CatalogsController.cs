using AutoMapper;
using Common.DataQueries;
using Common.DTOs.Catalog;
using Common.DTOs.Catalog.Catalogs;
using Common.DTOs.Catalog.Properties;
using Common.DTOs.Catalog.Units;
using Common.Repositories;
using Common.Specifications;
using Marketplace.Api.Validations;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.ProductsEntities;
using Marketplace.Data.Entities.PropertyEntities;
using Marketplace.Infrastructure.Mappers;
using Marketplace.Infrastructure.Services;
using Marketplace.Infrastructure.Specifications.Properties;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;

namespace Marketplace.Api.Controllers
{
    //[ApiController]
    [Route("/")]
    [Produces("application/json")]
    public class CatalogsController : ControllerBase
    {
        private readonly ILogger<CatalogsController> _logger;

        private readonly ICatalogService _catalogService;

        public CatalogsController(
            ICatalogService catalogService,
            ILogger<CatalogsController> logger)
        {
            _catalogService = catalogService;
            _logger = logger;
        }

        #region Upd-1

        [HttpGet("/")]
        public async Task<IActionResult> GetCatalogs()
        {
            _logger.LogInformation(nameof(GetCatalogs));

            return Ok(_catalogService.GetAllCatalogs());
        }

        [HttpGet("/properties/units")]
        public async Task<IActionResult> GetPropertyUnits()
        {
            _logger.LogInformation(nameof(GetPropertyUnits));

            return Ok(_catalogService.GetAllPropertyUnits());
        }

        [HttpGet("/properties")]
        public async Task<IActionResult> GetProperties(
           string? section_id,
           string? category_id)
        {
            _logger.LogInformation(nameof(GetProperties));

            return Ok(_catalogService.GetAllProperties(section_id, category_id));
        }

        [HttpPost("/")]
        public async Task<IActionResult> CreateCatalog(
            [FromBody]CatalogApiCreate model)
        {
            _logger.LogInformation(nameof(CreateCatalog));

            var validationResult = await ValidationRegistry.CatalogCreateValidator
                .ValidateAsync(model);

            if (!validationResult.IsValid)
                return BadRequest(ApiResponseRead<Catalog>.Failure(validationResult.Errors
                    .Select(er => er.ErrorMessage)));

            // metrics
            // test

            var res = await _catalogService.CreateCatalogAsync(model);

            return Ok(res.ToApiResponse());
        }

        [HttpPost("/properties/units")]
        public async Task<IActionResult> CreatePropertyUnit(
            [FromBody]CatalogPropertyUnitApiCreate model)
        {
            _logger.LogInformation(nameof(CreatePropertyUnit));

            // validate
            // metrics
            // test

            var res = await _catalogService.CreatePropertyUnitAsync(model);

            return Ok(res.ToApiResponse());
        }

        [HttpPost("/properties")]
        public async Task<IActionResult> CreateProperty(
            [FromBody]CatalogPropertyApiCreate model)
        {
            _logger.LogInformation(nameof(CreateProperty));

            // metrics
            // validation
            // test

            var res = await _catalogService.CreateCatalogPropertyAsync(model);

            return Ok(res.ToApiResponse());
        }

        #endregion
    }
}