﻿using Common.DTOs.Marketplace.Api;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class SectionPropertyValidator : AbstractValidator<SectionPropertyApiPost>
    {
        public SectionPropertyValidator()
        {
            RuleFor(x => x.PropertyId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));

            RuleFor(x => x.SectionId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));
        }
    }
}
