﻿namespace Marketplace.Api.Validations
{
    public class ValidationRegistry
    {
        public readonly static CatalogCreateValidator CatalogCreateValidator = new CatalogCreateValidator();

        public readonly static CategoryCreateValidator CategoryCreateValidator = new CategoryCreateValidator();

        public readonly static CategoryPropertyCreateValidator CategoryPropertyCreateValidator = new CategoryPropertyCreateValidator();

        public readonly static ProductCreateValidator ProductCreateValidator = new ProductCreateValidator();

        public readonly static CategorySectionCreateValidator CategorySectionCreateValidator = new CategorySectionCreateValidator();

        public readonly static SectionPropertyValidator SectionPropertyCreateValidator = new SectionPropertyValidator();
    }
}
