﻿using Common.DTOs.Marketplace;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class CategorySectionCreateValidator : AbstractValidator<CategorySectionApiPost>
    {
        public CategorySectionCreateValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .MinimumLength(2)
                .MaximumLength(60);

            RuleFor(x => x.CategoryId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));
        }
    }
}
