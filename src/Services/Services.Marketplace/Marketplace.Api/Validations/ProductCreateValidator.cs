﻿using Common.DTOs.ApiRequests;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class ProductCreateValidator : AbstractValidator<CatalogProductApiPost>
    {
        public ProductCreateValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(120);

            RuleFor(x => x.ExternalProductId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));

            RuleFor(x => x.Properties)
                .NotEmpty();
        }
    }
}
