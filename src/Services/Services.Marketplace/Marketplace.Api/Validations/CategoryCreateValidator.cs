﻿using Common.DTOs.Marketplace.Api;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class CategoryCreateValidator : AbstractValidator<CategoryApiPost>
    {
        public CategoryCreateValidator()
        {
            RuleFor(x => x.CatalogId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));

            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(60);
        }
    }
}
