﻿using Common.DTOs.Catalog.Catalogs;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class CatalogCreateValidator : AbstractValidator<CatalogApiCreate>
    {
        public CatalogCreateValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(60);
        }
    }
}
