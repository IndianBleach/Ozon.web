﻿using Common.DTOs.Marketplace.Api;
using FluentValidation;

namespace Marketplace.Api.Validations
{
    public class CategoryPropertyCreateValidator : AbstractValidator<CategoryPropertyApiPost>
    {
        public CategoryPropertyCreateValidator()
        {
            RuleFor(x => x.CategoryId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));

            RuleFor(x => x.PropertyId)
                .NotNull()
                .Must(x => Guid.TryParse(x, out _));
        }
    }
}
