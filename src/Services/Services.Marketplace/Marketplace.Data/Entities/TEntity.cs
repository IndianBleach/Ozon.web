﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marketplace.Data.Entities
{
    [PrimaryKey(nameof(Id))]
    public class TEntity : TServiceEntity
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }

    public abstract class TServiceEntity
    { }
}
