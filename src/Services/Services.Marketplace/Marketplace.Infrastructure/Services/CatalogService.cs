﻿using AutoMapper;
using Common.DataQueries;
using Common.DTOs.Catalog;
using Common.DTOs.Catalog.Catalogs;
using Common.DTOs.Catalog.Properties;
using Common.DTOs.Catalog.Units;
using Common.DTOs.Marketplace;
using Common.DTOs.Marketplace.Api;
using Common.Repositories;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.PropertyEntities;
using Marketplace.Infrastructure.Mappers;
using Marketplace.Infrastructure.Specifications.Properties;
using Marketplace.Infrastructure.Specifications.Sections;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marketplace.Infrastructure.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IServiceAsyncRepository<Catalog> _catalogRepository;

        private readonly IServiceAsyncRepository<CatalogProperty> _propertyRepository;

        private readonly IServiceAsyncRepository<CatalogPropertyUnit> _propertyUnitRepository;

        private readonly IServiceAsyncRepository<SectionProperty> _sectionPropertyRepository;

        private readonly IServiceAsyncRepository<CategoryProperty> _categoryPropertyRepository;

        private readonly IServiceAsyncRepository<CategorySection> _sectionsRepository;

        private readonly IServiceAsyncRepository<CatalogCategory> _categoryRepository;

        private readonly Mapper _mapper;

        public CatalogService(
            IServiceAsyncRepository<CatalogCategory> categoryRepository,
            IServiceAsyncRepository<CategorySection> sectionsRepository,
            IServiceAsyncRepository<CatalogProperty> propertyRepository,
            IServiceAsyncRepository<Catalog> catalogRepository,
            IServiceAsyncRepository<CatalogPropertyUnit> propertyUnitRepository,
            IServiceAsyncRepository<SectionProperty> sectionPropertyRepository,
            IServiceAsyncRepository<CategoryProperty> categoryPropertyRepository)
        {
            _categoryRepository = categoryRepository;
            _sectionsRepository = sectionsRepository;
            _sectionPropertyRepository = sectionPropertyRepository;
            _categoryPropertyRepository = categoryPropertyRepository;
            _propertyRepository = propertyRepository;
            _catalogRepository = catalogRepository;
            _propertyUnitRepository = propertyUnitRepository;

            var config = new MapperConfiguration(cfg => cfg.AddProfiles(new List<Profile> {
                new CatalogMapProfile(),
                new PropertyMapProfile()
            }));

            _mapper = new Mapper(config);
        }

        public async Task<QueryResult<Catalog>> CreateCatalogAsync(CatalogApiCreate model)
        {
            return await _catalogRepository.CreateAsync(new Catalog(
                model.Name));
        }

        public async Task<QueryResult<CatalogProperty>> CreateCatalogPropertyAsync(CatalogPropertyApiCreate model)
        {
            if (!_propertyUnitRepository.Any(x => x.Id == model.UnitId))
                return QueryResult<CatalogProperty>.Failure("property unit doenst exists");

            return await _propertyRepository.CreateAsync(new CatalogProperty(
                model.Name,
                model.UnitId,
                model.Desc));
        }

        public async Task<QueryResult<CategoryReadDto>> CreateCategoryAsync(CategoryApiPost model)
        {
            if (!_catalogRepository.Any(x => x.Id == model.CatalogId))
                return QueryResult<CategoryReadDto>.Failure("catalog not found");

            var query = await _categoryRepository.CreateAsync(new CatalogCategory(
                catalogId: model.CatalogId,
                name: model.Name));

            if (!query.IsSuccessed)
                return QueryResult<CategoryReadDto>.Failure(query.StatusMessage);

            return QueryResult<CategoryReadDto>.Successed(new CategoryReadDto
            { 
                CatalogId = query.Value.CatalogId,
                Id = query.Value.Id,
                Name = query.Value.Name
            });
        }

        public async Task<QueryResult<CategoryProperty>> CreateCategoryPropertyAsync(CategoryPropertyApiPost model)
        {
            if (!_categoryRepository.Any(x => x.Id == model.CategoryId))
                return QueryResult<CategoryProperty>.Failure("category not found");

            if (!_propertyRepository.Any(x => x.Id == model.PropertyId))
                return QueryResult<CategoryProperty>.Failure("property not found");

            return await _categoryPropertyRepository.CreateAsync(new CategoryProperty(
                categoryId: model.CategoryId,
                propertyId: model.PropertyId,
                isRequired: model.Required));
        }

        public async Task<QueryResult<CategorySectionRead>> CreateCategorySectionAsync(CategorySectionApiPost model)
        {
            if (!_categoryRepository.Any(x => x.Id == model.CategoryId))
                return QueryResult<CategorySectionRead>.Failure("category not found");

            var query = await _sectionsRepository.CreateAsync(new CategorySection(
                categoryId: model.CategoryId,
                name: model.Name));

            if (!query.IsSuccessed)
                return QueryResult<CategorySectionRead>.Failure(query.StatusMessage);

            return QueryResult<CategorySectionRead>.Successed(new CategorySectionRead
            { 
                CategoryId = query.Value.CategoryId,
                Id = query.Value.Id,
                Name = query.Value.Name
            });
        }

        public async Task<QueryResult<CatalogPropertyUnit>> CreatePropertyUnitAsync(CatalogPropertyUnitApiCreate model)
        {
            return await _propertyUnitRepository.CreateAsync(new CatalogPropertyUnit(
                model.Name));
        }

        public async Task<QueryResult<SectionProperty>> CreateSectionPropertyAsync(SectionPropertyApiPost model)
        {
            if (!_sectionsRepository.Any(x => x.Id == model.SectionId))
                return QueryResult<SectionProperty>.Failure("section not found");
            
            if (!_propertyRepository.Any(x => x.Id == model.PropertyId))
                return QueryResult<SectionProperty>.Failure("prop not found");

            return await _sectionPropertyRepository.CreateAsync(new SectionProperty(
                sectionId: model.SectionId,
                propertyId: model.PropertyId,
                isRequired: model.Required));
        }

        public IEnumerable<CatalogRead> GetAllCatalogs()
        {
            return _mapper.Map<IEnumerable<Catalog>, IEnumerable<CatalogRead>>(_catalogRepository.GetAll());
        }

        public IEnumerable<CatalogCategoryRead> GetAllCategories()
        {
            var res = _categoryRepository.GetAll();

            return _mapper.Map<IEnumerable<CatalogCategory>, IEnumerable<CatalogCategoryRead>>(res);
        }

        public IEnumerable<CatalogPropertyRead> GetAllProperties(string? section_id, string? category_id)
        {
            IEnumerable<CatalogProperty?> props;

            if (!string.IsNullOrEmpty(section_id) &&
                !string.IsNullOrEmpty(category_id))
            {
                props = Enumerable.Concat<CatalogProperty?>(
                    first: _categoryPropertyRepository.Find(
                        specification: new PropertiesOfCategorySpec(
                            categoryId: category_id,
                            onlyRequired: true))
                        .Select(x => x.Property),
                    second: _sectionPropertyRepository.Find(
                        specification: new PropertiesOfSectionSpec(
                            sectionId: section_id,
                            onlyRequired: true))
                        .Select(x => x.Property));

                props = props.Where(x => x != null);
            }
            else if (!string.IsNullOrEmpty(section_id))
            {
                props = _sectionPropertyRepository.Find(
                    specification: new PropertiesOfSectionSpec(
                        sectionId: section_id,
                        onlyRequired: true))
                    .Select(x => x.Property);
            }
            else if (!string.IsNullOrEmpty(category_id))
            {
                props = _categoryPropertyRepository.Find(
                    specification: new PropertiesOfCategorySpec(
                        categoryId: category_id,
                        onlyRequired: true))
                    .Select(x => x.Property);
            }
            else props = _propertyRepository.GetAll();

            return _mapper.Map<IEnumerable<CatalogProperty>, IEnumerable<CatalogPropertyRead>>(props);
        }

        public IEnumerable<CatalogPropertyUnit> GetAllPropertyUnits()
        {
            return _propertyUnitRepository.GetAll();
        }

        public IEnumerable<CatalogSectionRead> GetAllSections()
        {
            var items = _sectionsRepository.Find(new AllSectionsIncludeCategorySpec());

            return _mapper.Map<IEnumerable<CategorySection>, IEnumerable<CatalogSectionRead>>(items);
        }
    }
}
