﻿using Common.DataQueries;
using Common.DTOs.Catalog;
using Common.DTOs.Catalog.Catalogs;
using Common.DTOs.Catalog.Properties;
using Common.DTOs.Catalog.Units;
using Common.DTOs.Marketplace;
using Common.DTOs.Marketplace.Api;
using Marketplace.Data.Entities.CatalogEntities;
using Marketplace.Data.Entities.PropertyEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marketplace.Infrastructure.Services
{
    public interface ICatalogService
    {

        Task<QueryResult<CategorySectionRead>> CreateCategorySectionAsync(CategorySectionApiPost model);

        Task<QueryResult<SectionProperty>> CreateSectionPropertyAsync(SectionPropertyApiPost model);

        Task<QueryResult<CatalogProperty>> CreateCatalogPropertyAsync(CatalogPropertyApiCreate model);

        Task<QueryResult<CatalogPropertyUnit>> CreatePropertyUnitAsync(CatalogPropertyUnitApiCreate model);

        Task<QueryResult<Catalog>> CreateCatalogAsync(CatalogApiCreate model);

        Task<QueryResult<CategoryProperty>> CreateCategoryPropertyAsync(CategoryPropertyApiPost model);

        Task<QueryResult<CategoryReadDto>> CreateCategoryAsync(CategoryApiPost model);


        IEnumerable<CatalogCategoryRead> GetAllCategories();

        IEnumerable<CatalogSectionRead> GetAllSections();

        IEnumerable<CatalogPropertyRead> GetAllProperties(
           string? section_id,
           string? category_id);

        IEnumerable<CatalogRead> GetAllCatalogs();

        IEnumerable<CatalogPropertyUnit> GetAllPropertyUnits();
    }
}
