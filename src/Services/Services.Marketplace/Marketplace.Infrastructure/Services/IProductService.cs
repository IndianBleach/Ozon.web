﻿using Common.DataQueries;
using Common.DTOs.ApiRequests;
using Common.DTOs.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marketplace.Infrastructure.Services
{
    public interface IProductService
    {
        Task<QueryResult<string>> CreateProductAsync(CatalogProductApiPost model);

        Task<QueryResult<CatalogProductRead>> GetProductDetailByIdAsync(string productId);

        IEnumerable<CatalogProductShortRead> GetAllProducts();
    }
}
